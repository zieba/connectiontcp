<?php
/**
 * Klasa Server jest odpowiedzialna za połączenie z serwerem iHomeServer
 *
 * @author Master
 */
class Server {

    private $host;
    private $port;
    private $socket;

    function __construct($host, $port) {
        $this->host = $host;
        $this->port = $port;
        $this->socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP) or die("[" . date('d-m-Y H:i:s') . "] Could not create socket\n");
        socket_bind($this->socket, '127.0.0.1', 80) or die("[" . date('d-m-Y H:i:s') . "] Could not bind to socket\n"); // source na razie na sztywno
    }

    public function connect() {
        echo "Nawiązywanie połączenia w trakcie...\n";
        socket_connect($this->socket, $this->host, $this->port) or die("[" . date('d-m-Y H:i:s') . "] Could not set up connection\n");
        //Tutaj może test połączenia
        echo "Połączenie zostało nawiązane. \n";
    }

    public function disconnect() {
        socket_close($this->socket);
        echo "Połączenie zostało zakończone. \n";
    }

    public function test($request) {
        socket_write($this->socket, $request);
        $reply = socket_read($this->socket, 1024);
        return $reply;
    }



}
